package de.dhbw.vs.jprak.imagebtn;

import java.awt.Dimension;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;



@SuppressWarnings("serial")
public class ImageButton extends JButton {

	final private ImageIcon imageIcon;

	public ImageButton(String iconPath, String name, String descrition) {
		super(name);
		imageIcon = new ImageIcon(ImageButton.class.getResource(iconPath), descrition);

		scaleIcon(imageIcon);

		this.setIcon(imageIcon);
		this.setPreferredSize(new Dimension(50, 30));
		this.setOpaque(true);
		this.setText("");
	}

	private void scaleIcon(ImageIcon imageIcon) {
		double width, height;
		double relWidth, relHeight;
		double skalierungsFaktor;

		width = imageIcon.getIconWidth();
		height = imageIcon.getIconHeight();

		relWidth = 200 / width;
		relHeight = 50 / height;

		skalierungsFaktor = relWidth < relHeight ? relWidth : relHeight;

		imageIcon.setImage(imageIcon.getImage().getScaledInstance((int) (skalierungsFaktor * width),
				(int) (skalierungsFaktor * height), Image.SCALE_SMOOTH));
	}
}
