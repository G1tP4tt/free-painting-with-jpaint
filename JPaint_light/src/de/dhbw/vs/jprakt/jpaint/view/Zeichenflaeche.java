package de.dhbw.vs.jprakt.jpaint.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import de.dhbw.vs.jprakt.jpaint.drawing.Kreis;
import de.dhbw.vs.jprakt.jpaint.drawing.Rechteck;
import de.dhbw.vs.jprakt.jpaint.drawing.Stift;
import de.dhbw.vs.jprakt.jpaint.drawing.StretchElement;
import de.dhbw.vs.jprakt.jpaint.drawing.ZeichenElement;
import de.dhbw.vs.jprakt.jpaint.view.JPaintFenster.Modus;

@SuppressWarnings("serial")
public class Zeichenflaeche extends JPanel implements MouseMotionListener,
		MouseListener {

	private Point startStretch;
	private JPaintFenster fenster;
	

	private final ArrayList<ZeichenElement> elemente;
	

	public Zeichenflaeche(JPaintFenster fenster) {

		this.setBackground(Color.WHITE);
		this.fenster = fenster;
		this.addMouseListener(this);
		this.addMouseMotionListener(this);

		elemente = new ArrayList<ZeichenElement>();
	}

	@Override
	public void paintComponent(Graphics graphics) {
		super.paintComponent(graphics);

		for (ZeichenElement element : elemente) {
			element.paint(graphics);
		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if (fenster.getModus() == Modus.Stift) {
			elemente.add(new Stift(arg0.getPoint(), fenster.getColor(),fenster.getZeichenStaerke()));
			repaint();
		} else {
			elemente.remove(elemente.size() - 1);
			StretchElement element = null;
			if (fenster.getModus() == Modus.Rechteck) {
				element = new Rechteck(startStretch, arg0.getPoint(),
						fenster.getColor(),fenster.getZeichenStaerke());
			} else if (fenster.getModus() == Modus.Kreis) {
				element = new Kreis(startStretch, arg0.getPoint(),
						fenster.getColor(),fenster.getZeichenStaerke());
			}
			elemente.add(element);
			repaint();
		}

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		if (fenster.getModus() != Modus.Stift) {
			startStretch = arg0.getPoint();
			StretchElement element = null;
			if (fenster.getModus() == Modus.Rechteck) {
				element = new Rechteck(arg0.getPoint(), arg0.getPoint(),
						fenster.getColor(),fenster.getZeichenStaerke());
			} else if (fenster.getModus() == Modus.Kreis) {
				element = new Kreis(arg0.getPoint(), arg0.getPoint(),
						fenster.getColor(),fenster.getZeichenStaerke());
			}
			elemente.add(element);
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
}
