package de.dhbw.vs.jprakt.jpaint.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.dhbw.vs.jprak.imagebtn.*;



@SuppressWarnings("serial")
public class JPaintFenster extends JFrame {

	public enum Modus {
		Stift, Rechteck, Kreis;
	}

	private Modus modus = Modus.Stift;
	private Color color = Color.BLACK;
	private int zeichenStaerke = 3;
	

	private static Color[] colors = new Color[] { Color.BLACK, Color.RED,
			Color.BLUE, Color.YELLOW };

	public JPaintFenster() {
		super();

		
		JSlider slider = new JSlider(JSlider.HORIZONTAL,1,10,3);
		ImageButton stift = new ImageButton(
				"/de/dhbw/vs/jprakt/jpaint/view/stift.png", "Stift", "");
		ImageButton rechteck = new ImageButton(
				"/de/dhbw/vs/jprakt/jpaint/view/rechteck.png", "Rechteck", "");
		ImageButton kreis = new ImageButton(
				"/de/dhbw/vs/jprakt/jpaint/view/kreis.png", "Kreis", "");

		stift.setPreferredSize(new Dimension(100, 60));
		rechteck.setPreferredSize(new Dimension(100, 60));
		kreis.setPreferredSize(new Dimension(100, 60));
		slider.setPreferredSize(new Dimension(150,60));

		stift.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modus = Modus.Stift;
			}
		});

		slider.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				 JSlider source = (JSlider)e.getSource();
				 System.out.println(source.getValue());
				 zeichenStaerke = source.getValue();				 
			}
			
		});
		
		rechteck.addActionListener(e -> modus = Modus.Rechteck);
		kreis.addActionListener(e -> modus = Modus.Kreis);
		

		JPanel toolbar = new JPanel();

		toolbar.add(stift);
		toolbar.add(rechteck);
		toolbar.add(kreis);
		toolbar.add(slider);

		for (final Color currColor : colors) {
			JButton colorButton = new JButton();
			colorButton.setPreferredSize(new Dimension(100, 60));
			colorButton.setOpaque(true);
			colorButton.setBackground(currColor);
			colorButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					color = currColor;
				}
			});
			toolbar.add(colorButton);
		}

		this.setLayout(new BorderLayout());
		this.add(toolbar, BorderLayout.NORTH);
		this.add(new Zeichenflaeche(this), BorderLayout.CENTER);

		this.setSize(new Dimension(800, 600));
		this.setLocation(200, 200);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public Modus getModus() {
		return modus;
	}

	public Color getColor() {
		return color;
	}

	public int getZeichenStaerke() {
		return zeichenStaerke;
	}

	public void setZeichenStaerke(int zeichenStaerke) {
		this.zeichenStaerke = zeichenStaerke;
	}
}
