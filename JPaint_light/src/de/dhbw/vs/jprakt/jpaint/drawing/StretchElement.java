package de.dhbw.vs.jprakt.jpaint.drawing;

import java.awt.Color;
import java.awt.Point;


public abstract class StretchElement extends ZeichenElement {
	
	protected int x;
	protected int y;
	protected int width;
	protected int height;

	public StretchElement(Point startPoint, Point endPoint, Color farbe, int strength) {
		super(farbe, strength);
		
		if(startPoint.y<=endPoint.y) {
			y=startPoint.y;
		}
		else {
			y=endPoint.y;
		}
		height=Math.abs(endPoint.y-startPoint.y);
		
		if(startPoint.x<=endPoint.x) {
			x=startPoint.x;
		}
		else {
			x=endPoint.x;
		}
		width=Math.abs(endPoint.x-startPoint.x);
	}
}
