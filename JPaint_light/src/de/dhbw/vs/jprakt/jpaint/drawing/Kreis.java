package de.dhbw.vs.jprakt.jpaint.drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Kreis extends StretchElement {

	public Kreis(Point stretchStartPoint, Point stretchEndPoint, Color farbe, int strength) {
		super(stretchStartPoint, stretchEndPoint, farbe, strength);
	}

	@Override
	public void paint(Graphics graphics) {
		graphics.setColor(color);
		graphics.drawOval(x, y, width, height);
	}

}
