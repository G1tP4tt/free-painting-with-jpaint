package de.dhbw.vs.jprakt.jpaint.drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Rechteck extends StretchElement {

	public Rechteck(Point startPoint, Point endPoint, Color farbe, int strength) {
		super(startPoint, endPoint, farbe, strength);
	}

	@Override
	public void paint(Graphics graphics) {
		graphics.setColor(color);
		graphics.drawRect(x, y, width, height);
	}
}
