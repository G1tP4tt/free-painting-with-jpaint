package de.dhbw.vs.jprakt.jpaint.drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Stift extends ZeichenElement {

	private final Point point;


	public Stift(Point point, Color color, int strength) {
		super(color, strength);
		this.point = point;
	}

	@Override
	public void paint(Graphics graphics) {
		graphics.setColor(color);
		graphics.fillOval(point.x, point.y, 3, 3);
	}

}
