package de.dhbw.vs.jprakt.jpaint.drawing;

import java.awt.Color;
import java.awt.Graphics;

public abstract class ZeichenElement {

	protected final Color color;
	protected  int strength;
	
	public ZeichenElement(Color color, int strength) {
		super();
		this.color = color;
		this.strength = strength;
	}
	
	public abstract void paint(Graphics graphics);

}
